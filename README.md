# Introduction
This project provides a few basic examples of Nokia SRLinux gRPC/gNMI capabilities using '[gNMIc](https://gnmic.kmrd.dev/)' toolset.
## 1. Requirements
### 1.1 'gnmic' tool
- Install 'gNMIc' tool on a Client system.
    ```
    curl -sL https://github.com/karimra/gnmic/raw/master/install.sh | sudo bash
    ```
    > **Note:** For detailed information visit official page: https://gnmic.kmrd.dev

- Populate **"certs/"** sub-directory with Root CA and Client Certificates 
  - Example
  ```sh
  certs/
  ├── ca_extended.cert.pem
  └── client.cert.pem
  ```
  > Note: Don't forget to update configuration files with a proper certificate names!
  
### 1.2 SRLinux requirements
- Create TLS profile and activate gnmi-server on a target SRLinux system
  ```sh
  enter candidate
  /
  # info from running / system tls server-profile tls-self-1
      system {
          tls {
              server-profile srl-tls-profile {
                  key "-----BEGIN RSA PRIVATE KEY-----
          <place_private_key_here>
  -----END RSA PRIVATE KEY-----"
                  certificate "-----BEGIN CERTIFICATE-----
          <place_certificate_here>
  -----END CERTIFICATE-----"
                  trust-anchor "-----BEGIN CERTIFICATE-----
          <place_root_CA_certificate_here>
  -----END CERTIFICATE-----"
                  authenticate-client false
              }
          }
      }

  # info from running / system gnmi-server
      system {
          gnmi-server {
              admin-state enable
              timeout 7200
              rate-limit 60
              session-limit 20
              commit-confirmed-timeout 0
              network-instance mgmt {
                  admin-state enable
                  use-authentication true
                  port 57400
                  tls-profile srl-tls-profile
              }
            unix-socket {
                admin-state disable
                use-authentication true
            }
          }
      }
  commit stay

  # Verification
  info from state / system gnmi-server network-instance * | as table
  ```
- Example
  ```sh
  # info from state / system gnmi-server network-instance * | as table
  +-------------------------------------------------------------------+-------------+---------------+--------------------+-------+-------------------------------------------------------------------+
  |                               Name                                | Admin-state |  Oper-state   | Use-authentication | Port  |                            Tls-profile                            |
  +===================================================================+=============+===============+====================+=======+===================================================================+
  | mgmt                                                              | enable      | up            | false              | 57400 | srl-tls-profile                                                   |
  +-------------------------------------------------------------------+-------------+---------------+--------------------+-------+-------------------------------------------------------------------+
  ```
## 2. Preparation phase
- Before issuing any command, edit "config.yaml" file, where ALL session parameters are defined
  - Default username/password: admin/admin
  - Default port is 57400
  - 'skip-verify' option does Server certificate IP SAN extension verification against real IP address of the server, if there is a mismatch, request is rejected 
  - Optional "debug" option could be used during debugging :)
  - Multiple targets could be used to execute the same actions on multiple systems at the same time
- Example
  ```yaml
  # Inspired by https://github.com/karimra/gnmic/blob/master/config.yaml
  targets:
    138.203.15.89

  username: admin
  password: admin
  port: 57400
  timeout: 10s
  tls-cert: certs/swiss.cert.pem
  tls-ca: certs/ca.cert.pem
  #skip-verify: false
  skip-verify: true
  encoding: json_ietf
  debug: true
  no-prefix: true
  ```

## 3. SRLinux examples

### Get
- Get gNMI server status
  ```sh
  gnmic \
      --config config.yaml \
      get \
      --path "/system/gnmi-server/admin-state"
  ```
- Get info about "Network-Instance default" interfaces
  ```sh
  gnmic \
      --config config.yaml \
      get \
      --path "/network-instance[name=default]/interface[name=ethernet*]"
  ```
- Get info about operational state of "Network-Instance default" interfaces
  ```sh
  gnmic \
      --config config.yaml \
      get \
      --path "/network-instance[name=default]/interface[name=ethernet*]/oper-state"
  ```
- Get info about operational state of interface-1/1 subinterface 1
  ```sh
  gnmic \
      --config config.yaml \
      get \
      --path "/interface[name=ethernet-1/1]/subinterface[index=1]/admin-state"
  ```

### Update
- Set SRLinux hostname to "new-gnmic-name" value
  ```sh
  gnmic \
      --config config.yaml \
      set \
      --update /system/name/host-name:::json_ietf:::new-gnmic-name
  ```
- Set SRLinux hostname using configration file in json format
  ```sh
  gnmic \
      --config config.yaml \
      set \
      --update-path "/" \
      --update-file config/change_name.json
  ```
### Subscribe
- Example of a single telemetry subscription to get interface "ethernet-1/1" traffic-rate statistics
  ```sh
  gnmic \
      --config config.yaml \
      subscribe  \
      --path "/interface[name=ethernet-1/1]/traffic-rate"
  ```
  - Verify if subscription is registered on SRLinux system
    ```
    # info from state / system gnmi-server subscription *
      system {
          gnmi-server {
              subscription 164 {
                  user admin
                  user-agent grpc-go/1.30.0
                  remote-host 138.203.18.250
                  remote-port 49074
                  mode TARGET_DEFINED
                  start-time 2020-09-15T12:37:43.169Z
                  paths [
                      "interface[name=ethernet-1/1]/traffic-rate/..."
                  ]
              }
          }
      }
    ```
    ```
    # info from state / system gnmi-server subscription * | as table |filter fields *
    +------------+-------------------------------------------------------+-------------------------------------------------------+-----------------------------------------+-------------+----------------------+----------------+-------------------------------------------------------+-------------------------------------------------------+
    |     Id     |                         User                          |                      User-agent                       |               Remote-host               | Remote-port |   Sample-interval    |      Mode      |                      Start-time                       |                         Paths                         |
    +============+=======================================================+=======================================================+=========================================+=============+======================+================+=======================================================+=======================================================+
    |        164 | admin                                                 | grpc-go/1.30.0                                        | 138.203.18.250                          |       49074 |                      | TARGET_DEFINED | 2020-09-15T12:37:43.169Z                              | interface[name=ethernet-1/1]/traffic-rate/...         |
    +------------+-------------------------------------------------------+-------------------------------------------------------+-----------------------------------------+-------------+----------------------+----------------+-------------------------------------------------------+-------------------------------------------------------+
    ```
- Example of multiple subcriptions using config.yaml file
```sh
gnmic \
    --config config.yaml \
    subscribe
```
  - Subscription is managed by the following configuration inside config.yaml
    ```yaml
    # cat config.yaml
    # Inspired by https://github.com/karimra/gnmic/blob/master/config.yaml

    targets:
      138.203.15.89:
        subscriptions:
          #- system_facts
          - port_stats
          - service_state
          - bgp_neighbor_state

    username: admin
    password: admin
    port: 57400
    timeout: 10s
    tls-cert: certs/client.cert.pem
    tls-ca: certs/ca.cert.pem
    #skip-verify: false
    skip-verify: true
    encoding: json_ietf
    debug: true
    no-prefix: true

    # SRLinux subscription container
    subscriptions:
      port_stats:
        paths:
          - "/interface[name=ethernet-1/1]/traffic-rate"
          - "/interface[name=ethernet-1/2]/traffic-rate"
        mode: stream
        stream-mode: sample
        sample-interval: 5s
        encoding: json_ietf
      bgp_neighbor_state:
        paths:
          - "network-instance[name=default]/protocols/bgp/neighbor[peer-address=2000::*]/admin-state"
        mode: stream
        stream-mode: on-change
      service_state:
        paths:
          - "/network-instance[name=*]/admin-state"
        mode: stream
        stream-mode: on-change
      system_facts:
        paths:
          - "/system/name/host-name"
          - "/platform/control[slot=*]/software-version"
        mode: once

    ```
  - Verification
    ```
    # info from state / system gnmi-server subscription * | as table |filter fields *
    +------------+----------------------------------------------------------------------------+----------------------------------------------------------------------------+-----------------------------------------+-------------+----------------------+----------------+----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    |     Id     |                                    User                                    |                                 User-agent                                 |               Remote-host               | Remote-port |   Sample-interval    |      Mode      |                                 Start-time                                 |                                   Paths                                    |
    +============+============================================================================+============================================================================+=========================================+=============+======================+================+============================================================================+============================================================================+
    |        231 | admin                                                                      | grpc-go/1.30.0                                                             | 138.203.18.250                          |       49152 |                      | ON_CHANGE      | 2020-09-15T14:51:21.833Z                                                   | network-instance[name=*]/admin-state/...                                   |
    |        232 | admin                                                                      | grpc-go/1.30.0                                                             | 138.203.18.250                          |       49152 |                      | ON_CHANGE      | 2020-09-15T14:51:21.838Z                                                   | network-instance[name=default]/protocols/bgp/neighbor[peer-                |
    |            |                                                                            |                                                                            |                                         |             |                      |                |                                                                            | address=2000::*]/admin-state/...                                           |
    |        233 | admin                                                                      | grpc-go/1.30.0                                                             | 138.203.18.250                          |       49152 |                    5 | SAMPLE         | 2020-09-15T14:51:21.841Z                                                   | interface[name=ethernet-1/1]/traffic-rate/...                              |
    |            |                                                                            |                                                                            |                                         |             |                      |                |                                                                            | interface[name=ethernet-1/2]/traffic-rate/...                              |
    +------------+----------------------------------------------------------------------------+----------------------------------------------------------------------------+-----------------------------------------+-------------+----------------------+----------------+----------------------------------------------------------------------------+----------------------------------------------------------------------------+
    ```
